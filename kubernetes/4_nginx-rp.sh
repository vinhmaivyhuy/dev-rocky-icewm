#!/bin/bash

kubectl delete configmap rocky-certpem
kubectl create configmap rocky-certpem --from-file=./pem-files/certpem

kubectl delete configmap rocky-keypem
kubectl create configmap rocky-keypem --from-file=./pem-files/keypem

kubectl apply -f nginx-rp.yml
