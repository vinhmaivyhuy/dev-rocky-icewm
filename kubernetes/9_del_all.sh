#!/bin/bash

kubectl delete -f lb-nginx.yml
kubectl delete -f nginx-rp.yml
kubectl delete -f clusterips.yml
kubectl delete -f nodeports.yml
kubectl delete -f devs.yml
